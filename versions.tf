terraform {
  required_version = ">= 1.4.6"

  required_providers {
    gitlab = {
      version = "= 15.11.0"
      source  = "gitlabhq/gitlab"
    }
  }
}
