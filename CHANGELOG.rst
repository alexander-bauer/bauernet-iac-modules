#########
Changelog
#########

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[Unreleased]
============

[0.7.0] - 2023-09-30
====================

Changed
-----

* Add ``social-modules`` repository to default list.

[0.6.0] - 2023-07-29
====================

Changed
-----

* Add ``health-modules`` repository to default list.

[0.5.0] - 2023-07-11
====================

Changed
-----

* Set default access level for collaborators to ``developer``

[0.4.0] - 2023-07-11
====================

Added
-----

* Added ``frontend-modules`` to standard list

Removed
-------

* Removed ``homeassistant-modules`` until cleanup can be scheduled

[0.3.0] - 2023-05-02
====================

Added
-----

* Remaining repositories for original BauerNet reorganization to the default list:
  * ``authzn-modules``
  * ``bootstrapping-modules``
  * ``homeassistant-modules``
  * ``k8snet-modules``
  * ``kairos-modules``
  * ``observability-modules``
  * ``paperless-modules``
  * ``storage-modules``
  * ``unifi-modules``
  * ``vaultwarden-modules``

[0.2.0] - 2023-05-01
====================

Added
-----

* ``collaborators`` feature for granting access to repositories

[0.1.1] - 2023-05-01
====================

Changed
-------

* Repository privacy is now controlled by ``private`` rather than ``internal``, and the
  repository setting is changed to match.

[0.1.0] - 2023-05-01
====================

Added
-----

* Top-level module with sensible defaults
* ``repositories`` module for managing GitLab repositories
