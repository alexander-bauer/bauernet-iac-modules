variable "extra_repositories" {
  description = "Extra repositories to create. Names will be prefixed."
  type = list(object({
    name    = string
    private = optional(bool)
  }))
  default = []
}

variable "collaborators" {
  description = "Collaborators to add as members to projects"
  type = list(object({
    name       = string
    maintainer = optional(bool)
  }))
  default = []
}
