# -*- coding: utf-8 -*-
project = "BauerNet IaC Modules"
author = "Alexander Bauer"
copyright = copyright_since(author, '2023')

highlight_language = "terraform"
terraform_source = "../"
