####################
BauerNet IaC Modules
####################

This repository contains Terraform modules for managing IaC resources for BauerNet. This
includes repositories.

.. toctree::
   :maxdepth: 1
   :glob:

   */README
   CHANGELOG
