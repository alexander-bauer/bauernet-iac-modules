module "repositories" {
  source = "./repositories"

  prefix             = "bauernet-"
  extra_repositories = var.extra_repositories
  collaborators      = var.collaborators
}
