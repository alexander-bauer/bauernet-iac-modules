variable "prefix" {
  description = "String to prefix every repository name with."
  type        = string
}

variable "base_repositories" {
  description = "Primary list of repositories to create. Names will be prefixed."
  type = list(object({
    name    = string
    private = optional(bool)
  }))
  default = [
    { name = "authzn-modules" },
    { name = "bootstrapping-modules" },
    { name = "social-modules" },
    { name = "frontend-modules" },
    { name = "health-modules" },
    { name = "iac-modules" },
    { name = "k8snet-modules" },
    { name = "kairos-modules" },
    { name = "library" },
    { name = "observability-modules" },
    { name = "paperless-modules" },
    { name = "storage-modules" },
    { name = "unifi-modules" },
    { name = "vaultwarden-modules" },
  ]
}

variable "extra_repositories" {
  description = "Extra repositories to create. Names will be prefixed."
  type = list(object({
    name    = string
    private = optional(bool)
  }))
  default = []
}

variable "collaborators" {
  description = "Collaborators to add as members to projects"
  type = list(object({
    name       = string
    maintainer = optional(bool)
  }))
  default = []
}
