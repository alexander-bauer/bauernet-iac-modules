locals {
  all_repositories = {
    for r in concat(var.base_repositories, var.extra_repositories) :
    r.name => r
  }
  collaborators = { for c in var.collaborators : c.name => c }

  collaborator_product = {
    for pair in setproduct(keys(local.collaborators), keys(local.all_repositories)) :
    "${pair[0]}:${pair[1]}" => {
      collaborator = local.collaborators[pair[0]]
      project      = local.all_repositories[pair[1]]
    }
  }
}

data "gitlab_user" "collaborator" {
  for_each = local.collaborators
  username = each.key
}

resource "gitlab_project" "repository" {
  for_each = local.all_repositories
  name     = "${var.prefix}${each.key}"

  visibility_level = each.value.private == true ? "private" : "public"
}

resource "gitlab_branch_protection" "repository" {
  for_each = gitlab_project.repository
  project  = each.value.id
  branch   = each.value.default_branch

  merge_access_level     = "maintainer"
  push_access_level      = "maintainer"
  unprotect_access_level = "maintainer"

  allow_force_push = true # users with push access only
}

resource "gitlab_project_membership" "member" {
  for_each = local.collaborator_product

  project_id   = gitlab_project.repository[each.value.project.name].id
  user_id      = data.gitlab_user.collaborator[each.value.collaborator.name].id
  access_level = each.value.collaborator.maintainer == true ? "maintainer" : "developer"
}
